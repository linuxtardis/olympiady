$pdflatex = 'pdfcsplain -file-line-error %O %S';
$bibtex_mode = 0;
$dvi_mode = 0;
$ps_mode = 0;
$pdf_mode = 1;
$use_make_for_missing_files = 0;
$pdf_previewer = 'atril';
