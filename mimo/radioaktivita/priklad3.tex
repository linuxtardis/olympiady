  \thisis{Příklad 3:}
\vskip.5em
\par\bgroup
\advance\leftskip by2em
\advance\rightskip by2em
Poločas rozpadu nuklidu \kobalt~je $5.26$ roku.
Na počátku jsme měli $30$ gramů vzorku, které obsahovalo $20\;\%$ tohoto nuklidu.
Jaká je aktivita tohoto vzorku?
Atomová hmotnost \kobalt~je $59$. {\em [vsuvka: toto bude platit pro celý vzorek, nikoliv pro čistý izotop, ten by měl 60]}.
Po jaké době zbude ve vzorku $13\,000$ atomů tohoto nuklidu?
Kolik neutronů a protonů má dceřinné jádro, rozpadá-li se tento nuklid rozpadem $\beta^-$?
\par\egroup
\vskip.5em

Předem si zmíníme rovnici pro radioaktivní rozpad.
Ta nám říká, kolik ($N$) radioaktivních částic nám ještě zbylo.
Dospět k~ní můžeme relativně jednoduše.
\begitems\style
o~* Určitě výsledný počet částic bude nějak záviset na původním počtu částic $N_0$.
* Bude tam exponenciální funkce se základem jedna polovina, protože hovoříme o~{\em polo}času rozpadu.
* Když uplyne poločas rozpadu, zbude nám polovina částic.
  Naopak hned na začátku budeme mít ještě původní počet částic.
  V~argumentu exponenciální funkce tedy bude podíl $\frac{t}{T}$, kde $t$ je uplynulý čas a $T$ je poločas rozpadu.
  Ověření: V~exponentu při uplynutí poločasu rozpadu zůstane jednička, čili výsledkem by byl původní počet částic dělený dvěma.
\enditems
$$ N(t) = N_0 \cdot \left( \frac{1}{2}\right)^{\displaystyle\frac{t}{T}}$$

Podobný vztah platí také pro rychlost rozpadu, čili pro {\em aktivitu zářiče} (= kolik rozpadů nastane za sekundu).
Pokud předchozí vztah zderivujeme (a vynásobíme mínus jedničkou), dostaneme:
$$\eqalignno{
A(t) &= N_0\cdot \frac{\ln 2}{T} \cdot \left( \frac{1}{2}\right)^{\displaystyle\frac{t}{T}} \cr
A_0  &= N_0\cdot \frac{\ln 2}{T} \cr
A(t) &= A_0 \cdot \left( \frac{1}{2}\right)^{\displaystyle\frac{t}{T}} \cr
}$$
Aktivita zářiče se udává v jednotce Becquerel ($\Bq$), která má stejný rozměr jako hertz -- $\frac{1}{\sek}$, čili počet událostí za sekundu.

K řešení nejprve potřebujeme zjistit počet radioaktivních částic \kobalt~ve vzorku.
K tomu využijeme tzv. molární/atomovou hmotnost.
Číslo $59$ tak, jak je v zadání, nám říká, kolik gramů bude vážit jeden mol látky.
{\em Pozn: chemikářka nám dala takovou pomůcku -- \uv{ožralý} gram na mol...}
Obecně tedy molární hmotnost bude vyjádřená vzorcem ($M$~...~molární hmotnost, $m$~...~hmotnost vzorku, $N$~...~látkové množství):
$$ M = \frac{m}{n} $$

Protože částic v jednom gramu jakékoli látky je velmi mnoho, zavádí se veličina {\em látkové množství} ($n$).
Ta nám určuje, kolikrát vzorek s $N$ částicemi obsahuje $6.022\cdot10^{23}$ částic (toto číslo se nazývá Avogadrova konstanta $N_A$).
Hodnota Avogadrovy konstanty odpovídá počtu částic v 12 gramech uhlíku \uhlik.
Díky tomu můžeme rozumně vyjadřovat \uv{normální} množství látek.
$$\eqalignno{
1\;\mol &= \{N_A\}\;\hbox{částic} = 6.022\cdot10^{23}\;\hbox{\rm částic} \cr
n &= \frac{N}{N_A} \cr
}$$

Ke zjištění molárního množství vzorku tedy potřebujeme vydělit jeho hmotnost molární hmotností a výsledek vynásobit zadanými $20\;\%$.
Výsledek pak vynásobíme Avogadrovou konstantou, abychom dostali počet částic.
$$\eqalignno{
n &= \frac{m}{M} \cdot 20\;\%\cr
N_0 &= N_A \cdot \frac{m}{M} \cdot 20\;\%
}$$

Jako první potřebujeme zjistit aktivitu zářiče. Budeme uvažovat počáteční podmínky, potřebujeme tedy zjistit $A_0$.
$$\eqalignno{
A_0 &= N_0\cdot \frac{\ln 2}{T} \cr
A_0 &= N_A \cdot \frac{m}{M} \cdot \frac{\ln 2}{T} \cdot 20\;\%
}$$

Poločas rozpadu si vyjádříme v sekundách, aby nám správně vyšly jednotky.
$$\eqalignno{
T &= 5.26\;{\rm let} \cr
T &= 5.26 \cdot 365 \cdot 86\,400\;\sek \cr
T &= 165\,879\,360\;\sek \cca 166 \cdot 10^6\;\sek
}$$

Dosadíme.
$$\eqalignno{
A_0 &= 6.022\cdot10^{23}\;\mol^{-1} \cdot \frac{30\;\gram}{59\;\gmol} \cdot \frac{\ln 2}{166 \cdot 10^6\;\sek} \cdot 20\;\% \cr
A_0 &\cca 2.56 \cdot 10^{14}\;\Bq = 256\;\tera\Bq
}$$

{\bf Aktivita vzorku na začátku bude 256 terabecquerelů. }

Dále máme zjistit, za jak dlouho zbude ve vzorku 13\,000 atomů. Zkusíme tedy zpětně zjistit $t$ z rovnice pro radioaktivní rozpad.
$$\eqalignno{
N &= N_0 \cdot \left( \frac{1}{2}\right)^{\displaystyle\frac{t}{T}} \cr
\frac{N}{N_0} &= \left( \frac{1}{2}\right)^{\displaystyle\frac{t}{T}} \cr
\log_{\frac{1}{2}}\frac{N}{N_0} &= \displaystyle\frac{t}{T} \cr
t &= T \cdot \log_{\frac{1}{2}}\frac{N}{N_0} \cr
t &= T \cdot \frac{\ln N - \ln N_0}{-\ln 2} \cr
t &= T \cdot \frac{\ln N_0 - \ln N}{\ln 2} \cr
}$$

Dosadíme za původní počet částic.
$$\eqalignno{
t &= T \cdot \frac{\ln \left( N_A \cdot \frac{m}{M} \cdot 20\;\% \right) - \ln N}{\ln 2} \cr
t &= T \cdot \frac{\ln \left( \frac{N_A}{N} \cdot \frac{m}{M} \cdot 20\;\% \right)}{\ln 2} \cr
}$$

Dosadíme konkrétní hodnoty. Tady ale uděláme malou změnu -- poločas rozpadu dosadíme v původních jednotkách, čili v rocích.
Poté nebudeme muset převádět zpět ze sekund na roky.
$$\eqalignno{
t &= 5.26\;{\rm let} \cdot \frac{\ln \left( \frac{6.022\cdot10^{23}\;\mol^{-1}}{13\,000} \cdot \frac{30\;\gram}{59\;\gmol} \cdot 20\;\% \right)}{\ln 2} \cr
t &\cca 326.3\;{\rm let} \cr
}$$

{\bf Vzorek bude mít 13\,000 atomů \kobalt~za 326,3 let.}

Poslední otázkou je, kolik protonů a neutronů bude mít látka potom, co se rozpadne.
Bude se přitom rozpadat tzv. $\beta^-$ rozpadem.
Při něm z jádra atomu vyletí elektron (částice beta), který je záporně nabitý elementárním nábojem $-e$.

Reakci můžeme zkusit odhadnout.
Pokud se žádný náboj neztratil, musela vzniknout nějaká částice kladně nabitá nábojem $+e$.
V jádře se nacházejí pouze nenabité neutrony a kladně nabité protony.
Mohli bychom tedy předpokládat, že se nějaký neutron rozpadnul na proton a elektron.

Ve skutečnosti tomu opravdu tak je -- při $\beta^-$ rozpadu se neutron rozpadne na kladně nabitý proton, záporně nabitý elektron a také na elektronové antineutrino (to nás ale nezajímá).
\bgroup
\em
\typosize[8/10]
Naopak $\beta^+$ rozpad odpovída rozpadu protonu na neutron, pozitron (\uv{antielektron}) a elektronové neutrino.
\egroup

Značka \kobalt~nám říká, že jádro má 60 nukleonů a 27 z nich jsou protony.
Zbylých 33 částic tvoří neutrony.
Proběhnul-li beta rozpad, jádro nyní bude mít o neutron méně a o proton více.
Bude tedy mít 32 neutronů a 28 protonů.


\url{http://fyzika.jreichl.com/main.article/view/802-radioaktivita} \nl
\url{http://fyzika.jreichl.com/main.article/view/807-aktivita-zarice-a-rozpadovy-zakon} \nl
\url{http://fyzika.jreichl.com/main.article/view/804-zareni-beta} \nl
\url{http://fyzika.jreichl.com/main.article/view/573-relativni-atomova-hmotnost-latkove-mnozstvi}

