#!/usr/bin/python3

from data import *

import matplotlib        as mpl
import matplotlib.pyplot as plt
import numpy             as np

SPINE_COLOR = 'black'

def latexify(fig_width=None, fig_height=None, columns=1):
    """Set up matplotlib's RC params for LaTeX plotting.
    Call this before plotting a figure.

    Parameters
    ----------
    fig_width : float, optional, inches
    fig_height : float,  optional, inches
    columns : {1, 2}
    """

    # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

    # Width and max height in inches for IEEE journals taken from
    # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

    assert(columns in [1,2])

    if fig_width is None:
        fig_width = 3.39*2.54 if columns==1 else 6.9*2.54 # width in inches

    if fig_height is None:
        golden_mean = (np.sqrt(5)-1.0)/2.0    # Aesthetic ratio
        fig_height = fig_width*golden_mean # height in inches

    params = {#'backend': 'pdf',
              'text.latex.preamble': [r'\usepackage{gensymb}',
                                      r'\usepackage{lmodern}',
                                      r'\usepackage[czech]{babel}',
                                      r'\usepackage[T1]{fontenc}'],
              'text.latex.unicode': True,
              #'axes.labelsize': 8, # fontsize for x and y labels (was 10)
              #'axes.titlesize': 8,
              #'font.size': 8, # was 10
              #'legend.fontsize': 8, # was 10
              #'xtick.labelsize': 8,
              #'ytick.labelsize': 8,
              'text.usetex': True,
              'figure.figsize': [fig_width/2.54,fig_height/2.54],
              'font.family': 'serif'
    }

    mpl.rcParams.update(params)


def format_axes(ax):

    for spine in ['top', 'right']:
        ax.spines[spine].set_visible(False)

    for spine in ['left', 'bottom']:
        ax.spines[spine].set_color(SPINE_COLOR)
        ax.spines[spine].set_linewidth(0.5)

    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_tick_params(direction='out', color=SPINE_COLOR)

    return ax

latexify(fig_width=26.0)

fig, ax = plt.subplots(ncols=1, nrows=1)
#fig = plt.figure()
#ax = fig.add_axes([0,0,1,1])

format_axes(ax)
ax.set_title("Graf měření")
ax.set_xlabel("Odpor $R$ / \ohm")
ax.set_ylabel("Napětí $U$ / V ~~|~~ Výkon $P$ / W ")
ax.plot(R, U,  "y.-", label='Napětí $U$')
ax.plot(R, Ur, "b.-", label='Napětí $U_R$')
ax.plot(R, Uz, "g.-", label='Napětí $U_Z$')
ax.plot(R, P,  "r.-", label='Výkon $P$')
ax.grid(b=True, which='major', axis='both', color='gray', linestyle='-', visible=True, linewidth=0.5)
ax.grid(b=True, which='minor', axis='both', color='gray', linestyle=':', visible=True, linewidth=0.25)
ax.minorticks_on()
fig.legend(loc='center right')
fig.savefig("vykon_napeti.pdf", bbox_inches='tight')
