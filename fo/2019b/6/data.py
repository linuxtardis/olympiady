#!/usr/bin/python3

import numpy as np
import csv

N = []
U = []
I = []
Ur = []
Uz = []
P = []
R = []

with open("impedance.csv", "r") as f:
    reader = csv.reader(f)
    for row, record in enumerate(reader):
        if len(record) != 7:
            print(f"skipping bugged {record}")
        else:
            N.append(float(record[0]))
            I.append(float(record[1]))
            U.append(float(record[2]))
            Ur.append(float(record[3]))
            Uz.append(float(record[4]))
            R.append(float(record[5]))
            P.append(float(record[6]))

def maxPower():
    idx = 0
    pmax = 0
    for cur_idx, cur_power in enumerate(P):
        if pmax < cur_power:
            pmax = cur_power
            idx  = cur_idx

    return pmax, idx

def PFactor(pU, pUz, pUr):
    return (pU**2 + pUr**2 - pUz**2) / (2 * pU * pUr)

def Pcelk(pU, pUz, pUr, pI):
    return pU * pI * (pU**2 + pUr**2 - pUz**2) / (2 * pU * pUr)

def Eta(pU, pUz, pUr):
    return (2 * pUr**2) / (pU**2 + pUr**2 - pUz**2)

def CoilPhi(pU, pUz, pUr):
    return np.arccos((pU**2 - pUr**2 - pUz**2)/(2 * pUr * pUz)) / np.pi * 180.0

def PureR(pU, pUz, pUr, pI):
    return (pU**2 - pUr**2 - pUz**2) / (2 * pUr * pI)

def PureZ(pU, pUz, pUr, pI):
    return np.sqrt((2 * pUr * pUz)**2 - (pU**2 - pUr**2 - pUz**2)**2 ) / (2 * pUr * pI)

def PureL(pU, pUz, pUr, pI, freq):
    return np.sqrt((2 * pUr * pUz)**2 - (pU**2 - pUr**2 - pUz**2)**2 )/(4 * np.pi * pUr * pI * freq)

if __name__ == '__main__':
    Pmax, PmaxIdx = maxPower()
    Umax = U[PmaxIdx]
    Urmax = Ur[PmaxIdx]
    Uzmax = Uz[PmaxIdx]
    Imax = I[PmaxIdx]

    pf = PFactor(Umax, Uzmax, Urmax)
    angle = np.arccos(pf)/np.pi*180.0
    pcelk = Pcelk(Umax, Uzmax, Urmax, Imax)
    eta = Eta(Umax, Uzmax, Urmax)

    fi = CoilPhi(Umax, Uzmax, Urmax)
    r = PureR(Umax, Uzmax, Urmax, Imax)
    z = PureZ(Umax, Uzmax, Urmax, Imax)
    L = PureL(Umax, Uzmax, Urmax, Imax, 50)

    print(f"PF    = {pf:.3f}")
    print(f"phi   = {angle:.1f}°")
    print(f"Pmax  = {Pmax:.1f} W")
    print(f"Pcelk = {pcelk:.1f} W")
    print(f"eta   = {eta * 100:.1f} % = {Pmax/pcelk * 100:.1f} %")
    print()
    print(f"φ = {fi:.1f}°")
    print(f"r = {r:.2f} Ω")
    print(f"z = {z:.1f} Ω")
    print(f"L = {L:.4f} H")
