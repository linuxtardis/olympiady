#!/usr/bin/env python3

import pint
import numpy as np
import matplotlib.pyplot as plt

unit = pint.UnitRegistry()
unit.setup_matplotlib()

G = 6.67408e-11 * (unit.m ** 3) * (unit.kg ** -1) * (unit.s ** -2)
M = 2e41 * unit.kg

coreSize = 4 * unit.kpc
discSize = 15 * coreSize
velocity = 240 * unit.km / unit.s

distances = np.linspace(4, 60, 1000) * unit.kpc

accels = velocity ** 2 / distances

accels2 = G * M / (distances ** 2)

plt.plot(distances.to('kpc'), accels.to('mm/hour^2'))
plt.plot(distances.to('kpc'), accels2.to('mm/hour^2'))
plt.show()