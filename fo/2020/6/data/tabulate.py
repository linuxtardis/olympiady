#!/usr/bin/env python3

from common import *
import sys

human_readable_names = {
    'alobal.csv': 'Alobal',
    'lestene_drevo.csv': 'Leštěné dřevo (plovoucí podlaha)',
    'papirova_uterka.csv': 'Papírová utěrka',
    'silikonova_podlozka.csv': 'Silikonová modelovací podložka',
    'koberec.csv': 'Koberec',
    'papir.csv': 'Kancelářský papír A4',
    'dlazba.csv': 'Interiérová dlažba',
}


def main():
    if len(sys.argv) != 2:
        print(f'Usage: {sys.argv[0]} <set name>.csv', file=sys.stderr, flush=True)
        sys.exit(1)

    name = sys.argv[1]
    data = load_data(name)

    averages = {}
    absdeltas = {}
    reldeltas = {}
    for column in data.columns:
        h = float(column) / 100.0
        l = stick_length / 100.0

        Fs = []
        for x in data[column]:
            x_ = x / 100.0
            a = get_alpha(h, x_)
            b = get_beta(h, x_, l)
            f = get_friction_coefficient(a, b)
            Fs.append(f)

        Fst = pd.DataFrame(data=Fs, columns=['f'], dtype=float)
        f = get_average_value(Fst, 'f')
        Df = get_average_delta(Fst, 'f', f)
        df = get_relative_delta(f, Df)

        averages[column] = f
        absdeltas[column] = Df
        reldeltas[column] = df

    human_readable_name = human_readable_names[name]

    print(f"\\par{{\\bf {human_readable_name}: }}\\par\\nobreak")
    print("\\smallskip")
    print("\\centerline{")
    print("\\table{m|mmmmm}{")
    print("  ", end='')
    for column in data.columns:
        print("& ", end='')
        print(column, end='')
        print("\\,\\centi\\metr ", end='')
    print('\\crll')
    for i in range(0, len(data)):
        row = data.iloc[i]
        print(str(i+1) + " ", end='')
        for column in data.columns:
            value = row[column]
            print("& ", end='')
            print(value, end='')
            print("\\,\\centi\\metr ", end='')
        if i < len(data) - 1:
            print("\\cr")
        else:
            print("\\crl")

    print("\\overline{\\f} ", end='')
    for column in data.columns:
        print("& ", end='')
        print(f"{averages[column]:.4f} ", end='')
    print("\\cr")

    print("\\overline{\\Delta\\f} ", end='')
    for column in data.columns:
        print("& ", end='')
        print(f"\\pm{absdeltas[column]:.4f} ", end='')
    print("\\cr")

    print("\\delta\\f ", end='')
    for column in data.columns:
        print("& ", end='')
        print(f"\\pm{reldeltas[column]*100.0:.1f}\\,\\% ", end='')
    print("\\cr")

    print("}}")
    print("\\medskip")


if __name__ == '__main__':
    main()
