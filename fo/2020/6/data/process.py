#!/usr/bin/env python3

from common import *
import sys


def main():
    if len(sys.argv) != 2:
        print(f'Usage: {sys.argv[0]} <set name>.csv', file=sys.stderr, flush=True)
        sys.exit(1)

    name = sys.argv[1]
    data = load_data(name)
    for column in data.columns:
        avg_value = get_average_value(data, column)
        avg_delta = get_average_delta(data, column, avg_value)
        rel_delta = get_relative_delta(avg_value, avg_delta)

        h = float(column) / 100.0
        x = avg_value / 100.0
        l = stick_length

        alpha = get_alpha(h, x)
        beta = get_beta(h, x, l)
        f = get_friction_coefficient(alpha, beta)

        print(f"Results for '{name}' for col '{column}':")
        print(f"- h      = {100*h} cm")
        print(f"  avg(x) = {avg_value:.2f} cm")
        print(f"  absDx  = {avg_delta:.2f} cm")
        print(f"  relDx  = {rel_delta * 100:.2f} %")
        print(f"  x      = {100*x:.2f} cm")
        print(f"  l      = {100*l:.2f} cm")
        print(f"  alpha  = {np.degrees(alpha):.1f} °")
        print(f"  beta   = {np.degrees(beta):.1f} °")
        print(f"  f      = {f:.3f}")

    for column in data.columns:
        fs = []
        for row in data[column]:
            h = float(column) / 100.0
            x = row / 100.0
            l = stick_length / 100.0

            alpha = get_alpha(h, x)
            beta = get_beta(h, x, l)
            f = get_friction_coefficient(alpha, beta)
            fs.append(f)
        pd_fs = pd.DataFrame(data=fs, columns=['f'], dtype=float)
        print(pd_fs)
        avg_f = get_average_value(pd_fs, 'f')
        avg_df = get_average_delta(pd_fs, 'f', avg_f)
        rel_df = get_relative_delta(avg_f, avg_df)
        print(f"Accumulated results for col {column}:")
        print(f"avg(f) = {avg_f:.3f}")
        print(f"absDf  = {avg_df:.3f}")
        print(f"relDf  = {rel_df*100:.1f} %")


if __name__ == '__main__':
    main()
