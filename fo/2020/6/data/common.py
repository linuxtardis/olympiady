import pandas as pd
import numpy as np
import os

stick_length = 83.0


def load_data(name):
    path = os.path.join(os.path.dirname(__file__), name)
    return pd.read_csv(filepath_or_buffer=path,
                       sep=',',
                       header=0,
                       index_col=None)


def get_alpha(h, x):
    return np.arctan(x/h)


def get_beta(h, x, l):
    return np.arccos(np.sqrt(h ** 2 + x ** 2) / (2.0 * l))


def get_friction_coefficient(alpha, beta):
    return ( np.sin(alpha - beta) * np.sin(alpha + beta) ) \
           / (np.sin(2.0 * beta) - np.sin(alpha - beta) * np.cos(alpha + beta))


def get_average_value(table, column):
    return table[column].mean()


def get_average_delta(table, column, avg_value):
    return (table[column] - avg_value).abs().mean()


def get_relative_delta(avg_value, avg_delta):
    return avg_delta / avg_value
