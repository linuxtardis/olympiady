* Máme dokázat, že součinitel smykového tření lze vyjádřit následujícím vztahem.
$$ \f
= \frac{\sin(\alfa-\beta) \sin(\alfa+\beta)}{\sin2\beta - \sin(\alfa-\beta) \cos(\alfa+\beta)}
= \frac{\cos 2\beta - \cos 2\alfa}{3\sin2\beta - \sin2\alfa} $$

Tento úkol jsem řešil rozborem sil působících na dřevěnou tyč. Nakreslil jsem si proto náčrtek.\nl
{\em Poznámka}: vektory na obrázku nemají kvůli přehlednosti porovnatelnou velikost.
\medskip
\centerline{\hbox{
\input obrazky/nacrtek2
}}
\centerline{\hbox{
\label[nacrtek2]\captionJeej/f Přehled sil\endcapJeej
}}
\vfil\break
Na tyč budou působit tyto síly:
\begitems\style n
* Vlákno bude působit svojí tahovou silou $\vec\Fv$.
Tato síla bude mířit vždy ve směru vlákna -- vlákno nedokáže přenášet moment.
Zároveň tato síla bude mít právě takovou velikost, jakou na ni působí tyč. Vlákno se také nebude prodlužovat a bude tvořit délkově pevný úchyt.
Díky to se bude \uv{střed otáčení} pro posuzování momentů sil nacházet právě v~místě úchytu tyče k~provázku.

Tuto sílu si rozložíme do vodorovného směru jako $\vec\Fvt$ a do svislého směru jako $\vec\Fvn$.
Díky tomu bude možné porovnat směrové složky s~ostatními silami.

* Na tyč bude působit tíhová síla $\vec\Fg$.
Protože tyč je homogenní, bude síla působit v~těžišti nacházejícím se v~půlce délky tyče.
Její velikost znát přímo nepotřebujeme; síla ale bude mířit svisle dolů.

* Jako poslední bude na tyč působit kombinace $\vec\Fp$ třecí síly $\vec\Fpt$ a reakce podložky $\vec\Fpn$.
Reakce podložky $\vec\Fpn$ bude mířit svisle vzhůru a její velikost bude právě taková, jak silně bude svisle působit tyč na podložku.
Třecí síla $\vec\Fpt$ naproti tomu bude mířit vodorovně směrem od stěny.
Její velikost bude ale také záviset na velikosti reakce podložky -- (maximální) velikost třecí síly bude $\Fpt = \f \Fpn$.
\enditems

Jakmile během experimentu nastane rovnováha, bude dřevěná tyč v~klidu.
To znamená, že se nebude ani posouvat (tj. vektorový součet sil bude nulový), ani otáčet (tj. vektorový součet momentů sil bude nulový).
$$\eqalignno{
\sum \vec\F &= \vec0 & \label[soucet_sil] \eqmark \cr
\sum \vec\M &= \vec0 & \label[soucet_momentu] \eqmark
}$$

Pro splnění podmínky \ref[soucet_sil] musí platit, že se mezi sebou vyruší složky v~jednotlivých směrech.
$$\eqalignno{
\Fpt &= \Fvt        & \label[horizontaly] \eqmark \cr
\Fg  &= \Fpn + \Fvn & \label[vertikaly] \eqmark
}$$

Dále víme, že velikost třecí síly $\Fpt$ bude nepřímo záviset na velikosti síly $\Fpn$.
$$\eqalignno{
\Fpt  &= \f \Fpn & \label[treci] \eqmark
}$$

Ke splnění podmínky \ref[soucet_momentu] budeme muset vyjádřit velikosti momentů jednotlivých sil.
Nebudeme s~nimi počítat vektorově -- směrově totiž momenty budou mířit vždy buď ven z~nebo do nákresny, zajímá nás tedy pouze znaménko/smysl otáčení.
Budeme přitom vycházet z definice momentu síly \ref[moment_sily] a z velikosti vektorového součinu \ref[velikost_crossu].

Jak jsem již zmínil výše, střed otáčení je tvořen úchytem tyče k provázku. Síla $\vec\Fv$ bude tedy mít nulový moment.

Tíhová síla ale již svůj moment mít bude.
Polohový vektor bude mířit z horního konce tyče do její půlky.
S ním bude tíhová síla svírat uhel $180\stup-\alfa-\beta$.
$$ \vec\Mg = \vec\Fg \cross \vec\rg $$
$$ \Mg = \Fg \cdot \frac{\l}{2} \cdot \sin(180\stup-\alfa-\beta) $$
%
Můžeme opět upravit argument sinu tak, aby neobsahoval rozdíl.
$$\eqalignno{
\Mg &= \frac{1}{2} \Fg \cdot \l \cdot \sin(\alfa+\beta)
}$$

Jako další si určíme moment síly $\Fp$.
Polohový vektor v tomto případě bude začínat na horním konci tyče a končit na opačném, bude tedy mít délku $\l$.
Síla s tímto vektorem bude svírat úhel $\alfa+\beta+\arctg\f$.

{\em Proč zrovna $\arctg\f$?} Je to dané velikostí třecí síly.
Funkce tangens je definovaná jako poměr délky protilehlé odvěsny ku délce přilehĺé odvěsny v pravoúhlém trojúhelníku.
Proto platí:
$$ \tg x = \frac{\Fpt}{\Fpn} = \frac{\f\Fpn}{\Fpn} = \f $$
Po použití inverzní funkce dostáváme velikost úhlu $x$.
$$ x = \arctg\f $$

Potřebovat budeme také velikost síly $\Fp$. Tu zjistíme pomocí Pythagorovy věty a vztahu \ref[treci].
$$\eqalignno{
\Fp &= \sqrt{\Fpt^2 + \Fpn^2} = \sqrt{\f^2\Fpn^2 + \Fpn^2} = \Fpn \cdot \sqrt{\f^2 + 1}
}$$

Pojďme si tedy odvodit moment síly $\Fp$.
$$ \vec\Mp = \vec\Fp \cross \vec\rp $$
$$\eqalignno{
\Mp &= \Fpn \cdot \sqrt{\f^2 + 1} \cdot \l \cdot \sin(\alfa+\beta+\arctg\f) \cr
}$$
Sinus rozložíme pomocí vzorce pro součet v argumentu \ref[soucet_v_sinu].
$$\eqalignno{
\Mp &= \Fpn \cdot \sqrt{\f^2 + 1} \cdot \l \cdot \left( \sin(\alfa+\beta)\cdot\cos(\arctg\f) + \cos(\alfa+\beta)\cdot\sin(\arctg\f) \right)     \cr
}$$
Dále použijeme vztahy \ref[sin_arctg] a \ref[cos_arctg] a roznásobíme odmocninou.
$$\eqalignno{
\Mp &= \Fpn \cdot \sqrt{\f^2 + 1} \cdot \l \cdot \left( \sin(\alfa+\beta)\cdot\frac{1}{\sqrt{\f^2+1}} + \cos(\alfa+\beta)\cdot\frac{\f}{\sqrt{\f^2+1}} \right)     \cr
\Mp &= \Fpn \cdot \l \cdot \left( \sin(\alfa+\beta) + \f\cos(\alfa+\beta) \right)     \cr
}$$

Nyní zapíšeme a upravíme rovnost mezi momenty $\Mp$ a $\Mg$ -- oba míři opačným směrem, jejich velikost tedy musí být shodná.
$$\eqalignno{
\Mg &= \Mp \cr
\frac{1}{2} \Fg \cdot \l \cdot \sin(\alfa+\beta) &= \Fpn \cdot \l \cdot \left( \sin(\alfa+\beta) + \f\cos(\alfa+\beta) \right)     \cr
\Fpn &= \frac{1}{2} \Fg \cdot \frac{\sin(\alfa+\beta)}{\sin(\alfa+\beta) + \f\cos(\alfa+\beta)} & \label[normalovka] \eqmark \cr
}$$

Více rovnic již ze sil a momentů nedostaneme.
Pro konkrétní řešení ale potřebujeme ještě pátou rovnici.
Tu získáme přes poměr velikostí složek $\vec\Fvn$ a $\vec\Fvt$, který bude roven tangensu úhlu, který svírá síla $\vec\Fv$ s vodorovnou rovinou.
Pak rovnici upravíme, aby výrazy v argumentech funkcí byly již použity výše.
$$\eqalignno{
\frac{\Fvn}{\Fvt} &= \tg\left( 90\stup - \alfa + \beta\right) \cr
\frac{\Fvn}{\Fvt} &= \frac{ \sin( 90\stup - \alfa + \beta) }{ \cos( 90\stup - \alfa + \beta) } \cr
\frac{\Fvn}{\Fvt} &= \frac{ \cos(\alfa - \beta) }{ \sin( \alfa - \beta) } &\label[semifinale]\eqmark \cr
}$$

Dále dosadíme z rovnic \ref[horizontaly], \ref[vertikaly], \ref[treci] a \ref[normalovka] do této rovnice \ref[semifinale].
Odtamtud vyjádříme součinitel smykového tření $\f$.
$$\eqalignno{
\frac{ \cos(\alfa - \beta) }{ \sin( \alfa - \beta) } &= \frac{\Fg - \Fpn}{\f\Fpn}  \cr
\frac{ \cos(\alfa - \beta) }{ \sin( \alfa - \beta) } &= \frac{1}{\f} \cdot \left(\frac{\Fg}{\Fpn} - 1 \right) \cr
\frac{ \cos(\alfa - \beta) }{ \sin( \alfa - \beta) } &= \frac{1}{\f} \cdot \left(\frac{\Fg}{\frac{1}{2} \Fg \cdot \frac{\sin(\alfa+\beta)}{\sin(\alfa+\beta) + \f\cos(\alfa+\beta)}} - 1 \right) \cr
\frac{ \cos(\alfa - \beta) }{ \sin( \alfa - \beta) } &= \frac{1}{\f} \cdot \left(\frac{2\sin(\alfa+\beta) + 2\f\cos(\alfa+\beta)}{\sin(\alfa+\beta)} - 1 \right) \cr
\frac{ \cos(\alfa - \beta) }{ \sin( \alfa - \beta) } &= \frac{1}{\f} \cdot \frac{\sin(\alfa+\beta) + 2\f\cos(\alfa+\beta)}{\sin(\alfa+\beta)} \cr
}$$$$\eqalignno{
\f \cdot \cos(\alfa - \beta) \cdot \sin(\alfa+\beta)&=  \sin(\alfa+\beta)\cdot\sin( \alfa - \beta) + 2\f\cos(\alfa+\beta)\cdot\sin( \alfa - \beta) \cr
\f \cdot \cos(\alfa - \beta) \cdot \sin(\alfa+\beta)   -  2\f\cos(\alfa+\beta)\cdot\sin( \alfa - \beta)  &=  \sin(\alfa+\beta)\cdot\sin( \alfa - \beta) \cr
}$$$$\eqalignno{
\f &=  \frac{  \sin(\alfa+\beta)\cdot\sin( \alfa - \beta) }{ \cos(\alfa - \beta) \cdot \sin(\alfa+\beta)   -  2\cos(\alfa+\beta)\cdot\sin( \alfa - \beta) }\cr
\f &=  \frac{  \sin( \alfa - \beta) \cdot \sin(\alfa+\beta) }{ \sin2\beta - \sin( \alfa - \beta) \cdot \cos(\alfa+\beta) }\cr
}$$

Dostali jsme stejný vztah, jako nám předepsalo zadání.
Ještě bychom ale měli dokázat, že druhá, upravená forma odpovídá té první.
Obě tedy upravíme na společný výraz. Druhou variantu upravíme takto:
$$\eqalignno{
% prvni verze
\frac{\cos 2\beta - \cos 2\alfa}{3\sin2\beta - \sin2\alfa}
&= \frac{\cos^2\beta - \sin^2\beta - \cos^2\alfa + \sin^2\alfa}{6\sin\beta\cos\beta - 2\sin\alfa\cos\alfa} \cr
&= \frac{\sin^2\alfa + \cos^2\beta - 1}{3\sin\beta\cos\beta - \sin\alfa\cos\alfa} \cr
}$$
První variantu upravíme následovně.
$$\eqalignno{
% druha verze
\frac{\sin(\alfa-\beta) \sin(\alfa+\beta)}{\sin2\beta - \sin(\alfa-\beta) \cos(\alfa+\beta)}
&= \frac{ (\sin\alfa\cos\beta - \cos\alfa\sin\beta) (\sin\alfa\cos\beta + \cos\alfa\sin\beta)}{2\sin\beta\cos\beta - (\sin\alfa\cos\beta - \cos\alfa\sin\beta)(\cos\alfa\cos\beta - \sin\alfa\sin\beta) } \cr
&= \frac{      \sin^2\alfa\cos^2\beta - \cos^2\alfa\sin^2\beta      }{2\sin\beta\cos\beta - (\sin\alfa\cos\beta - \cos\alfa\sin\beta)(\cos\alfa\cos\beta - \sin\alfa\sin\beta) } \cr
&= \frac{      \sin^2\alfa\cos^2\beta - (1-\sin^2\alfa)(1-\cos^2\beta)      }{2\sin\beta\cos\beta - (\sin\alfa\cos\beta - \cos\alfa\sin\beta)(\cos\alfa\cos\beta - \sin\alfa\sin\beta) } \cr
&= \frac{      \sin^2\alfa+\cos^2\beta-1    }{2\sin\beta\cos\beta - (\sin\alfa\cos\beta - \cos\alfa\sin\beta)(\cos\alfa\cos\beta - \sin\alfa\sin\beta) } \cr
&= \frac{\sin^2\alfa+\cos^2\beta-1}{    2\sin\beta\cos\beta + (\cos\alfa\sin\beta - \sin\alfa\cos\beta)(\cos\alfa\cos\beta - \sin\alfa\sin\beta) } \cr
&= \frac{\sin^2\alfa+\cos^2\beta-1}{    2\sin\beta\cos\beta +       \cos^2\alfa\sin\beta\cos\beta - \sin\alfa\cos\alfa\cos^2\beta     -\cos\alfa\sin\alfa\sin^2\beta + \sin^2\alfa\cos\beta\sin\beta } \cr
&= \frac{\sin^2\alfa+\cos^2\beta-1}{    2\sin\beta\cos\beta +       (\cos^2\alfa+\sin^2\alfa)\cdot\sin\beta\cos\beta - (\cos^2\beta + \sin^2\beta)\cdot\sin\alfa\cos\alfa } \cr
&= \frac{\sin^2\alfa+\cos^2\beta-1}{    3\sin\beta\cos\beta - \sin\alfa\cos\alfa } \cr
}$$

Obě varianty se podařilo upravit na stejný výraz, dokázal jsem tedy, že vyjádření jsou ekvivalentní.
