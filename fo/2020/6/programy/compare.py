#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D

def my_fn(a, b):
	return ( np.sin(a-b) * np.sin(a+b) ) / ( np.cos(a-b) * np.sin(a+b) - 2 * np.sin(a-b) * np.cos(a+b) )

def their_fn(a, b):
	return ( np.sin(a-b) * np.sin(a+b) ) / ( np.sin(2*b) - np.sin(a-b) * np.cos(a+b) )
#	return ( np.cos(2*b) - np.cos(2*a) ) / ( 3*np.sin(2*b) - np.sin(2*a) )

va = np.linspace(0, np.pi/2, 100)
vb = np.linspace(0, np.pi/2, 100)


xs       = []
ys       = []
zs_my    = []
zs_their = []
for a in va:
	for b in vb:
		if a <= b or a == 0.0 or b == 0.0:
			continue
		xs.append(a)
		ys.append(b)
		my = np.clip(my_fn(a, b), -1.0, 1.0)
		their = np.clip(their_fn(a, b), -1.0, 1.0)
		zs_their.append(their)
		zs_my.append(my)
		if my != their:
			print(f"MISMATCH at a={np.degrees(a):5.2f} b={np.degrees(b):5.2f} delta={my-their:+6.3f}")

fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(xs, ys, zs_my,    zdir='z', c='blue', s=1)
ax.scatter(xs, ys, zs_their, zdir='z', c='red',  s=1)
ax.set_xlabel("a")
ax.set_ylabel("b")
ax.set_zlabel("f")
plt.show()
