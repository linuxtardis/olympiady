import numpy as np
import pint
import math

unit = pint.UnitRegistry()

lv = 2.3e6 * unit.J / unit.kg
o = 56 * unit.km
h = 100 * unit.m
M = 18e-3 * unit.kg / unit.mol
R = 8.314 * unit.J / unit.K / unit.mol
phi25 = 0.8
phi18 = 1.0
p25 = 3.17e3 * unit.Pa
p18 = 2.07e3 * unit.Pa
T25 = (273.15 + 25) * unit.K
T18 = (273.15 + 18) * unit.K


print("OLD RESULTS:")

radius = o / (2 * math.pi)
V = math.pi * radius ** 2 * h
m25 = (phi25 * p25) * V * M / (R * T25)
m18 = (phi18 * p18) * V * M / (R * T18)
Dm = m25 - m18
Lv = lv * Dm

print("Radius:       ", radius.to("m"))
print("Volume 1:     ", V.to("m**3"))
print("Volume 2:     ", V.to("m**3"))
print("Initial mass: ", m25.to("kg"))
print("Final mass:   ", m18.to("kg"))
print("Delta mass:   ", Dm.to("kg"))
print("Total heat:   ", Lv.to("TJ"))
print("Ratio:        ", (Lv / V).to("kJ/m**3"))

print("NEW RESULTS:")

T0 = T25
p0 = phi25 * p25
V0 = (o**2 * h) / (4 * math.pi) * (T25/T18)

T1 = T18
p1 = phi18 * p18
V1 = (o**2 * h) / (4 * math.pi) #* (T18/T25)

m25 = (p0 * V0 * M) / (R * T0)
m18 = (p1 * V1 * M) / (R * T1)


Dm = m25 - m18
Lv = lv * Dm

print("Volume 1:     ", V0.to("m**3"))
print("Volume 2:     ", V1.to("m**3"))
print("Initial mass: ", m25.to("kg"))
print("Final mass:   ", m18.to("kg"))
print("Delta mass:   ", Dm.to("kg"))
print("Total heat:   ", Lv.to("TJ"))
print("Temp ratio:   ", (T1/T0))
print("just volume:  ", "{:.3E}".format(V.to("m**3")))
